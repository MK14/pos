export interface NoteItem {
  title: string;
  body: string;
  link?: string;
  time: string;
}

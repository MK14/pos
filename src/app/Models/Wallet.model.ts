
export interface Wallet {
  frozenBalance: Number;
  orangeCacheBalance: Number;
  _id?: string;
}

export interface HistoryItem {
  type: string;
  number: string;
  value: string;
  time: string;
}

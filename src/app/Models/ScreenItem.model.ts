export interface ScreenItem {
  title: string;
  desc: string;
  link: string;
  externalURL?: string;
}

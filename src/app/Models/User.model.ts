export interface User {
  _id: string;
  avatarURL: string;
  username: string;
  providerID: string;
  phoneNumber: string;
  walletID: string;
}

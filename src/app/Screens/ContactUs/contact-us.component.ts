import { Component, OnInit } from '@angular/core';
import { LangService } from 'src/app/Services/lang.service';
import { timer } from 'rxjs';
import { Location } from '@angular/common';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
})
export class ContactUsComponent implements OnInit {

  dir: string = 'rtl';
  state: string = 'idial';
  emailReqErr: string = "";
  subjectReqErr: string = "";
  email: string;
  subject: string;
  body: string;


  constructor(
    private langService: LangService,
    private location: Location
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr'
  }

  changeEmail(email) {

    this.email = email;
  }

  changeSubject(subject) {
    this.subject = subject;
  }

  changeBody(body) {
    this.body = body;
  }

  submit() {
    if (!this.email || !this.subject) {

      if (!this.email) {
        this.emailReqErr = "emailReq";
      } else {
        this.emailReqErr = null;
      }
      if (!this.subject) {
        this.subjectReqErr = "subjectReq"
      } else {
        this.subjectReqErr = null;
      }
    } else {
      console.log("no Errors");
      this.emailReqErr = this.subjectReqErr = null;
      this.sendingMsg()
    }
  }

  sendingMsg() {
    this.state = 'sending';
    timer(3000).subscribe(() => {
      this.state = "success"
    })
  }
  actionModal() {
    this.state = 'idial';
    this.location.back();
  }

}

import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { ContactUsModule } from './contact-us.module';
import { ContactUsComponent } from './contact-us.component';



const routes: Routes = [
  { path: '', component: ContactUsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactUsRouting { }

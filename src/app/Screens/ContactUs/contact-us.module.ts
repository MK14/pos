import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/Shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { ContactUsRouting } from './contact-us.routing';
import { ContactUsComponent } from './contact-us.component';

@NgModule({
  declarations: [
    ContactUsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IonicModule,
    ContactUsRouting
  ]
})
export class ContactUsModule { }

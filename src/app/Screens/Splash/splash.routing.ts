import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { SplashComponent } from './splash.component';


const routes: Routes = [
  { path: '', component: SplashComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SplashRouting { }

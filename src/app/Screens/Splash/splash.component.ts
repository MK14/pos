import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { timer } from 'rxjs';
import { AuthService } from 'src/app/Services/auth.service';
import { ThrowStmt } from '@angular/compiler';
const { Network } = Plugins;



@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss'],
})
export class SplashComponent implements OnInit {

  activeModal: string;
  state: string = 'idial'

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthService
  ) { }

  ngOnInit() {
    // this.checkConnection();
    // this.changeNetStateListener();
    this.checkAuth()
  }


  checkAuth() {
    let user = localStorage.getItem("POSTrader");
    if (user) {
      this.auth.authenticated.next(true);
      this.router.navigate(['/main'])
    } else {
      this.router.navigate(['/login'])
    }
  }


  async checkConnection() {
    let status = await Network.getStatus();
    if (status.connectionType !== 'cellular') {
      console.log("Conn Type : ", status.connectionType);

      this.activeModal = 'wrongConnType';
    } else {
      this.checkAuthorize();
    }
  }

  async checkAuthorize() {
    // this.activeModal = 'AuthErr'
    this.router.navigate(['/login'], {
      relativeTo: this.route
    })
  }

  changeNetStateListener() {
    Network.addListener('networkStatusChange', (status) => {
      console.log("Network status changes :  ", status);
      let { connected, connectionType } = status;
      if (connectionType !== 'cellular') {
        this.activeModal = 'wrongConnType';
      }
    })
  }

  tryAgain() {
    // this.activeModal = null;
    this.state = 'loading';
    timer(1000).subscribe(() => {
      this.state = 'idial'
      this.checkConnection();
    })
  }

  // exitApp() {
  //   try {
  //     App.exitApp();
  //     // navigator['app'].exitApp();
  //   } catch (err) {
  //     console.log("ERRR : ", err);
  //   }
  // }



}

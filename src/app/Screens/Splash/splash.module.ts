import { NgModule } from "@angular/core";
import { SplashComponent } from './splash.component';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/Shared/shared.module';
import { CommonModule } from '@angular/common';
import { SplashRouting } from './splash.routing';

@NgModule({
  declarations: [
    SplashComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    SharedModule,
    SplashRouting
  ]
})
export class SplashModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loading: boolean = false;
  username: string = '';
  password: string = '';
  errMsg: string = '';
  usernameErr: string = '';
  passwordErr: string = '';

  constructor(
    private router: Router,
    private auth: AuthService
  ) { }

  ngOnInit() { }


  changeUserName(username) {
    this.username = username;
  }
  changePassword(password) {
    this.password = password;
  }

  clearErrors() {
    this.errMsg = '';
    this.passwordErr = '';
    this.usernameErr = '';
  }


  login() {

    this.clearErrors();
    console.log("THis :", this);


    if (!this.username && !this.passwordErr) {
      this.usernameErr = "usernameReq";
      this.passwordErr = "passwordReq";
    }

    else if (!this.username) {
      this.usernameErr = "usernameReq";
    }

    else if (!this.password) {
      this.passwordErr = "passwordReq";
    }

    else {
      this.loading = true;
      this.auth.login(this.username, this.password).subscribe(user => {
        console.log("Login Success : ", user);
        localStorage.setItem("POSTrader", JSON.stringify(user))
        this.loading = false;
        this.auth.authenticated.next(true);
        this.router.navigate(['/main']);

      }, (err) => {
        this.loading = false;
        console.log("Login Err Happened !!! ", err.error);
        this.errMsg = err?.error?.message;
      })
    }

  }

}

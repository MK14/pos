import { NgModule } from "@angular/core";
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/Shared/shared.module';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoginRouting } from './login.routing';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    SharedModule,
    LoginRouting
  ]
})
export class LoginModule { }

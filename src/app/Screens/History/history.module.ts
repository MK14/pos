import { NgModule } from "@angular/core";
import { HistoryRouting } from './history.routing';
import { HistoryComponent } from './history.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/Shared/shared.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    HistoryComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IonicModule,
    HistoryRouting
  ]
})
export class HistoryModule { }

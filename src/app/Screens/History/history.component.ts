import { Component, OnInit, OnDestroy } from '@angular/core';
import { HistoryItem } from 'src/app/Models/historyItem.model';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements OnInit, OnDestroy {

  history: HistoryItem[] = [
    {
      type: 'cash',
      number: '0123456789',
      time: '09:55 20-12-2020',
      value: '55'
    },
    {
      type: 'credit',
      number: '0123456789',
      time: '09:55 05-08-2020',
      value: '15'
    },
    {
      type: 'cash',
      number: '123456789',
      time: '09:55 10-12-2020',
      value: '155'
    }
  ]

  dir: string = 'rtl';
  dirSub: Subscription;

  constructor(
    private langService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
  }
  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

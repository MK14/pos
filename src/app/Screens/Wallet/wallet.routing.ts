import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { WalletComponent } from './wallet.component';


const routes: Routes = [
  { path: '', component: WalletComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WalletRouting { }

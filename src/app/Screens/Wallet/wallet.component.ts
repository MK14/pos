import { Component, OnInit, OnDestroy } from '@angular/core';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';
import { WalletService } from 'src/app/Services/wallet.service';
import { Wallet } from 'src/app/Models/Wallet.model';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss'],
})
export class WalletComponent implements OnInit, OnDestroy {

  dir: string = 'rtl';
  dirSub: Subscription;
  walletSub: Subscription;
  orangeCashBalance: Number = 0;
  walletBalance: Number = 0;
  loading = true;


  constructor(
    private langService: LangService,
    private wallet: WalletService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
    this.getCredit();
  }

  getCredit() {
    this.walletSub = this.wallet.getCredit().subscribe((data: Wallet) => {
      console.log("Wallet Data : ", data);
      this.loading = false;
      this.orangeCashBalance = data.orangeCacheBalance;
      this.walletBalance = data.frozenBalance;
    })
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

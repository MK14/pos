import { NgModule } from "@angular/core";
import { WalletComponent } from './wallet.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/Shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { WalletRouting } from './wallet.routing';

@NgModule({
  declarations: [
    WalletComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IonicModule,
    WalletRouting
  ]
})
export class WalletModule { }

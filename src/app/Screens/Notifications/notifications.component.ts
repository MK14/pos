import { Component, OnInit } from '@angular/core';
import { NoteItem } from 'src/app/Models/noteItem.model';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {

  notifications: NoteItem[] = [];

  constructor() { }

  ngOnInit() {
    this.notifications = [
      { title: "noteTitle", body: "noteBody", time: "20-12-2020" },
      { title: "noteTitle", body: "noteBody", time: "20-12-2020" },
      { title: "noteTitle", body: "noteBody", time: "20-12-2020" },
    ]
  }

}

import { NgModule } from "@angular/core";
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/Shared/shared.module';
import { CommonModule } from '@angular/common';
import { NotificationsComponent } from './notifications.component';
import { NotificationsRouting } from './notifications.routing';

@NgModule({
  declarations: [
    NotificationsComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    SharedModule,
    NotificationsRouting
  ]
})
export class NotificationsModule { }

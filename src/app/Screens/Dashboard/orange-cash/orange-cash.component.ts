import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-orange-cash',
  templateUrl: './orange-cash.component.html',
  styleUrls: ['./orange-cash.component.scss'],
})
export class OrangeCashComponent implements OnInit {

  state: string = 'idial';

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() { }

  showSuccessModal() {
    this.state = 'showSuccess';
  }
  hideModal() {
    this.state = 'idial'
  }

  actionModal() {
    this.router.navigate(['../'], {
      relativeTo: this.route
    })
  }

}

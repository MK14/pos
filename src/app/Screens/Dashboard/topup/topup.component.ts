import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-topup',
  templateUrl: './topup.component.html',
  styleUrls: ['./topup.component.scss'],
})
export class TopupComponent implements OnInit {

  state: string = 'idial';

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  showSuccessModal() {
    this.state = 'showSuccess';
  }
  hideModal() {
    this.state = 'idial'
  }
  handleAction() {
    this.router.navigate(['../'], {
      relativeTo: this.route
    })
  }

}

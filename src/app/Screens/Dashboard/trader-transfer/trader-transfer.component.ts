import { Component, OnInit } from '@angular/core';
import { SelectOption } from 'src/app/Models/SelectOption.model';

@Component({
  selector: 'app-trader-transfer',
  templateUrl: './trader-transfer.component.html',
  styleUrls: ['./trader-transfer.component.scss'],
})
export class TraderTransferComponent implements OnInit {

  operationOptions: SelectOption[] = [
    { name: 'orangeCash', value: 'cash' },
    { name: 'topup', value: 'topup' },
  ]

  constructor() { }

  ngOnInit() { }

}

import { Component, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform } from '@ionic/angular';

const { Browser } = Plugins;

@Component({
  selector: 'app-other-services',
  templateUrl: './other-services.component.html',
  styleUrls: ['./other-services.component.scss'],
})
export class OtherServicesComponent implements OnInit {

  url = 'https://www.orange.com/en';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private plateform: Platform,
  ) { }

  ngOnInit() {
    this.openBrawser();
    this.plateform.backButton.subscribeWithPriority(-1, () => {
      this.backToHome();
    })
  }

  openBrawser() {
    Browser.open({ url: this.url })
    Browser.addListener('browserFinished', () => {
      this.backToHome();
    })
  }

  backToHome() {
    this.router.navigate(['/main'], {
      relativeTo: this.route
    })
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ScreenItem } from 'src/app/Models/ScreenItem.model';
import { Subscription } from 'rxjs';
import { LangService } from 'src/app/Services/lang.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit, OnDestroy {

  services: ScreenItem[] = [
    { title: "gsmBillIndividuals", desc: "gsmBillIndividualsMsg", link: 'gsmindividual' },
    { title: "gsmBillCorporates", desc: "gsmBillCorporatesMsg", link: 'gsmcorporates' },
    { title: "dslBill", desc: "dslBillMsg", link: 'dslpayment' },
  ]

  dir: string = 'rtl';
  dirSub: Subscription;

  constructor(
    private langService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();

  }

}

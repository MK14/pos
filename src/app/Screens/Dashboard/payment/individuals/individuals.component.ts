import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-individuals',
  templateUrl: './individuals.component.html',
  styleUrls: ['./individuals.component.scss'],
})
export class IndividualsComponent implements OnInit {


  state: string = 'idial';
  phoneNum: any = '';
  errorMsg: string = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() { }

  changePhoneNum(num) {
    this.phoneNum = num;
    this.errorMsg = '';
  }
  checkNumData() {
    if (this.phoneNum) {
      this.state = "loading";
      timer(3000).subscribe(() => {
        if (this.state !== 'cancelled')
          this.state = 'userDataLoaded'
      })
    } else {
      this.errorMsg = 'mustEnterPhoneNum'
    }
  }

  cancel() {
    this.state = 'cancelled'
  }

  hideLoadModal() {
    this.state = 'idial';
  }
  payBill() {
    timer(3000).subscribe(() => {
      this.state = 'success'
    })
  }
  back() {
    this.state = 'idial';
    this.phoneNum = null;
    this.errorMsg = null;
    // this.router.navigate(['../'], {
    //   relativeTo: this.route
    // })
    this.location.back();
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer } from 'rxjs';
import { Location } from '@angular/common';

@Component({
  selector: 'app-corporate',
  templateUrl: './corporate.component.html',
  styleUrls: ['./corporate.component.scss'],
})
export class CorporateComponent implements OnInit {

  state: string = 'idial';
  codeNum: any = '';
  errorMsg: any = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() { }

  changeCodeNum(num) {
    this.codeNum = num;
    this.errorMsg = '';
  }
  checkNumData() {
    if (this.codeNum) {
      this.state = "loading";
      timer(3000).subscribe(() => {
        if (this.state !== 'cancelled')
          this.state = 'compDataLoaded'
      })
    } else {
      this.errorMsg = 'mustEnterCodeNum'
    }
  }

  cancel() {
    this.state = 'cancelled'
  }

  hideLoadModal() {
    this.state = 'idial';
  }
  payBill() {
    timer(3000).subscribe(() => {
      this.state = 'success'
    })
  }
  back() {
    this.state = 'idial';
    this.codeNum = null;
    this.errorMsg = null;
    // this.router.navigate(['../'], {
    //   relativeTo: this.route
    // })
    this.location.back();
  }

}

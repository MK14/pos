import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { PaymentComponent } from './payment.component';
import { IndividualsComponent } from './individuals/individuals.component';
import { DslPaymentComponent } from './dsl-payment/dsl-payment.component';
import { CorporateComponent } from './corporate/corporate.component';


const routes: Routes = [
  { path: '', component: PaymentComponent },
  { path: 'gsmindividual', component: IndividualsComponent },
  { path: 'dslpayment', component: DslPaymentComponent },
  { path: 'gsmcorporates', component: CorporateComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRouting { }

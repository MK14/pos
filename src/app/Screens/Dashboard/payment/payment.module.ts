import { NgModule } from "@angular/core";
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/Shared/shared.module';
import { CommonModule } from '@angular/common';
import { PaymentComponent } from './payment.component';
import { PaymentRouting } from './payment.routing';
import { IndividualsComponent } from './individuals/individuals.component';
import { DslPaymentComponent } from './dsl-payment/dsl-payment.component';
import { CorporateComponent } from './corporate/corporate.component';


@NgModule({
  declarations: [
    PaymentComponent,
    IndividualsComponent,
    DslPaymentComponent,
    CorporateComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    SharedModule,
    PaymentRouting
  ]
})
export class PaymentModule { }

import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { MainComponent } from './main/main.component';
import { TopupComponent } from './topup/topup.component';
import { OrangeCashComponent } from './orange-cash/orange-cash.component';
import { OtherServicesComponent } from './other-services/other-services.component';
import { TraderTransferComponent } from './trader-transfer/trader-transfer.component';
import { PaymentComponent } from './payment/payment.component';


const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'topup', component: TopupComponent },
  { path: 'cash', component: OrangeCashComponent },
  { path: 'others', component: OtherServicesComponent },
  { path: 'transfertrader', component: TraderTransferComponent },
  { path: 'payment', loadChildren: () => import('./payment/payment.module').then(m => m.PaymentModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRouting { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ScreenItem } from 'src/app/Models/ScreenItem.model';
import { Plugins } from '@capacitor/core';
import { Platform, IonRouterOutlet } from '@ionic/angular';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';

const { App } = Plugins;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit, OnDestroy {

  screens: ScreenItem[] = [
    {
      title: 'topup',
      desc: 'topupMsg'
      ,
      link: 'topup'
    },
    {
      title: 'orangeCash',
      desc: 'orangeCashMsg',
      link: 'cash'
    },
    {
      title: 'traderTrans',
      desc: 'traderTransMsg',
      link: 'transfertrader'
    },
    {
      title: 'paymentServices',
      desc: 'paymentServiceMsg',
      link: 'payment'
    },
    {
      title: 'otherServices',
      desc: 'otherServicesMsg',
      link: 'others',
      externalURL: 'https://www.orange.com/en'
    },
  ]

  dir: string = 'rtl';
  dirSub: Subscription;

  constructor(
    private plateform: Platform,
    private routerOutlet: IonRouterOutlet,
    private langService: LangService
  ) { }

  ngOnInit() {

    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
    this.plateform.backButton.subscribeWithPriority(-1, () => {
      if (!this.routerOutlet.canGoBack()) {
        App.exitApp();
      }
    })
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

import { NgModule } from "@angular/core";
import { Dashboard } from './dashboard.component';
import { DashboardRouting } from './dashboard.routing';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MainComponent } from './main/main.component';
import { SharedModule } from 'src/app/Shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { TopupComponent } from './topup/topup.component';
import { OrangeCashComponent } from './orange-cash/orange-cash.component';
import { OtherServicesComponent } from './other-services/other-services.component';
import { TraderTransferComponent } from './trader-transfer/trader-transfer.component';
import { PaymentComponent } from './payment/payment.component';

@NgModule({
  declarations: [
    Dashboard,
    MainComponent,
    TopupComponent,
    OrangeCashComponent,
    OtherServicesComponent,
    TraderTransferComponent,
  ],
  imports: [
    CommonModule,
    DashboardRouting,
    FormsModule,
    SharedModule,
    IonicModule
  ],
})

export class DashboardModule { }

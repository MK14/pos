import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  Plugins,
} from '@capacitor/core';
import { MobileNoteService } from './Services/pushNotifications.service';
import { TranslateService } from '@ngx-translate/core';
import { LangService } from './Services/lang.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Subscription } from 'rxjs';
import { AuthService } from './Services/auth.service';
import { ThrowStmt } from '@angular/compiler';


const { SplashScreen } = Plugins;




@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {

  menuSide: string = 'end';
  authSub: Subscription;
  authState = false;

  constructor(
    private mobileNoteService: MobileNoteService,
    private deviceService: DeviceDetectorService,
    private translate: TranslateService,
    private langService: LangService,
    private auth: AuthService

  ) { }

  ngOnInit() {

    this.menuSide = this.langService.getLang() === 'ar' ? 'end' : 'start'
    this.authSub = this.auth.authenticated.subscribe((status) => {
      this.authState = status;
    })


    this.translate.setDefaultLang(this.langService.getLang());
    console.log("is mobile : ", this.deviceService.os);

    if (this.deviceService.isMobile()) {
      this.mobileNoteService.requestPermission();
      this.mobileNoteService.notificationListeners();
    }
    SplashScreen.hide();
  }

  ngOnDestroy() {
    this.authSub.unsubscribe();
  }

}

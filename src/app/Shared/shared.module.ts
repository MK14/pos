import { NgModule } from "@angular/core";
import { SidemenuComponent } from '../Components/sidemenu/sidemenu.component';
import { MainHeaderComponent } from '../Components/main-header/main-header.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainItemComponent } from '../Components/main-item/main-item.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SubHeaderComponent } from '../Components/sub-header/sub-header.component';
import { AvailCreditComponent } from '../Components/avail-credit/avail-credit.component';
import { InputComponent } from '../Components/input/input.component';
import { MyBtnComponent } from '../Components/my-btn/my-btn.component';
import { IFrameComponent } from '../Components/iframe/iframe.component';
import { CashCardComponent } from '../Components/cash-card/cash-card.component';
import { CreditCardComponent } from '../Components/credit-card/credit-card.component';
import { StackIntroComponent } from '../Components/stack-intro/stack-intro.component';
import { DropDownComponent } from '../Components/drop-down/drop-down.component';
import { SuccessComponent } from '../Components/Alerts/success/success.component';
import { CashHistoryComponent } from '../Components/cash-history/cash-history.component';
import { CreditHistoryComponent } from '../Components/credit-history/credit-history.component';
import { TraderHistoryComponent } from '../Components/trader-history/trader-history.component';
import { ConnErrComponent } from '../Components/Alerts/conn-err/conn-err.component';
import { AuthErrComponent } from '../Components/Alerts/auth-err/auth-err.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { translateBrowserLoaderFactory } from './loaders/translate-browser.loader';
import { TransferState } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LoadDataComponent } from '../Components/Alerts/load-data/load-data.component';
import { UserDataComponent } from '../Components/Alerts/user-data/user-data.component';
import { CompanyDataComponent } from '../Components/Alerts/company-data/company-data.component';
import { NotificationsComponent } from '../Screens/Notifications/notifications.component';
import { NoteItemComponent } from '../Components/note-item/note-item.component';
import { TextAreaComponent } from '../Components/text-area/text-area.component';



@NgModule({
  declarations: [
    SidemenuComponent,
    MainHeaderComponent,
    MainItemComponent,
    SubHeaderComponent,
    AvailCreditComponent,
    InputComponent,
    MyBtnComponent,
    IFrameComponent,
    CashCardComponent,
    CreditCardComponent,
    StackIntroComponent,
    DropDownComponent,
    SuccessComponent,
    CashHistoryComponent,
    CreditHistoryComponent,
    TraderHistoryComponent,
    ConnErrComponent,
    AuthErrComponent,
    LoadDataComponent,
    UserDataComponent,
    CompanyDataComponent,
    NoteItemComponent,
    TextAreaComponent
  ],
  imports: [
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: translateBrowserLoaderFactory,
        deps: [HttpClient, TransferState]
      }
    }),
  ],
  exports: [
    SidemenuComponent,
    MainHeaderComponent,
    MainItemComponent,
    SubHeaderComponent,
    AvailCreditComponent,
    InputComponent,
    MyBtnComponent,
    IFrameComponent,
    CashCardComponent,
    CreditCardComponent,
    StackIntroComponent,
    DropDownComponent,
    SuccessComponent,
    CashHistoryComponent,
    CreditHistoryComponent,
    TraderHistoryComponent,
    ConnErrComponent,
    AuthErrComponent,
    TranslateModule,
    LoadDataComponent,
    UserDataComponent,
    CompanyDataComponent,
    NoteItemComponent,
    TextAreaComponent
  ],
  providers: [
    TransferState
  ]
})
export class SharedModule { }

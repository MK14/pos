import { trigger, state, style, transition, animate } from '@angular/animations';

export const Animations = {
  bounceUp: trigger('bounceUp', [
    state("void", style({
      top: "-11vh"
    })),
    transition("void <=> *", animate(250))
  ])
}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { WalletComponent } from './Screens/Wallet/wallet.component';
import { SplashComponent } from './Screens/Splash/splash.component';

const routes: Routes = [

  {
    path: '',
    loadChildren: () => import('./Screens/Splash/splash.module').then(m => m.SplashModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./Screens/Login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'history',
    loadChildren: () => import('./Screens/History/history.module').then(m => m.HistoryModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./Screens/Dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'wallet',
    loadChildren: () => import('./Screens/Wallet/wallet.module').then(m => m.WalletModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./Screens/Notifications/notifications.module').then(m => m.NotificationsModule)
  },
  {
    path: 'contactus',
    loadChildren: () => import('./Screens/ContactUs/contact-us.module').then(m => m.ContactUsModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { baseURL } from '../Shared/global';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authenticated = new Subject<boolean>()


  constructor(
    private http: HttpClient
  ) { }

  login(username, password) {
    let url = baseURL + "/user/login";
    return this.http.post(url, {
      username,
      password
    })
  }
}

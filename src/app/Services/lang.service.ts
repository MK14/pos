import { Injectable, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LangService implements OnInit {
  urlLang: string = "en";
  lang: string;
  dir = new Subject<string>()


  constructor(private router: Router, private trans: TranslateService) {
  }

  ngOnInit() {
  }
  intialization() {

    this.urlLang = this.router.url.substring(1, 3);
    if (this.urlLang) {
      this.trans.setDefaultLang(this.urlLang);
      localStorage.setItem('lang', this.urlLang);
    }

  }
  changeLanguage(lang) {
    let dir = lang === 'ar' ? 'rtl' : 'ltr';
    this.lang = lang;
    localStorage.setItem("lang", lang);
    this.trans.setDefaultLang(lang);
    this.dir.next(dir);
  }

  getLang() {
    // return this.router.url.substring(1, 3);
    if (localStorage.getItem('lang')) {
      return localStorage.getItem('lang');
    } else {
      return 'ar';
    }
  }



}

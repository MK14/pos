import { Injectable } from '@angular/core';
import { baseURL } from '../Shared/global';
import { HttpClient } from '@angular/common/http';
import { User } from '../Models/User.model';


@Injectable({
  providedIn: 'root'
})
export class WalletService {

  constructor(
    private http: HttpClient
  ) { }


  getCredit() {
    let user: User = JSON.parse(localStorage.getItem("POSTrader"));

    let url = `${baseURL}/wallet/${user.walletID}`;
    return this.http.get(url);
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'MyBtn',
  templateUrl: './my-btn.component.html',
  styleUrls: ['./my-btn.component.scss'],
})
export class MyBtnComponent implements OnInit {

  @Input('type') type: string = "filled";
  @Input('width') width: string = "100%";
  @Input('label') label: string;
  @Output('click') click = new EventEmitter();

  constructor() { }

  ngOnInit() { }

  handleClick() {
    this.click.emit();
  }

}

import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { SelectOption } from 'src/app/Models/SelectOption.model';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'DropDown',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss'],
})
export class DropDownComponent implements OnInit, OnDestroy {

  @Input('label') label: string;
  @Input('options') options: SelectOption[] = [];

  dir: string = 'rtl';
  dirSub: Subscription;

  constructor(
    private langService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

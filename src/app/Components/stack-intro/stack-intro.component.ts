import { Component, OnInit, Input } from '@angular/core';
import { title } from 'process';

@Component({
  selector: 'StackIntro',
  templateUrl: './stack-intro.component.html',
  styleUrls: ['./stack-intro.component.scss'],
})
export class StackIntroComponent implements OnInit {

  @Input('title') title: string;
  @Input('desc') desc: string;

  constructor() { }

  ngOnInit() { }

}

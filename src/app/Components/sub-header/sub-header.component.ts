import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';

@Component({
  selector: 'SubHeader',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.scss'],
})
export class SubHeaderComponent implements OnInit, OnDestroy {

  @Input('title') title;
  @Input('backStep') backStep;
  dir: string = 'rtl';
  dirSub: Subscription;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private langService: LangService,
    private location: Location
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'ltr' : 'rtl';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
  }

  back() {
    // this.router.navigateByUrl.
    if (this.backStep) {
      this.router.navigate(['../'], {
        relativeTo: this.route
      })
    } else {
      this.location.back();
    }
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

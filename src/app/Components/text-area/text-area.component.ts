import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { LangService } from 'src/app/Services/lang.service';

@Component({
  selector: 'MyTextArea',
  templateUrl: './text-area.component.html',
  styleUrls: ['./text-area.component.scss'],
})
export class TextAreaComponent implements OnInit {

  state: string = "idial";
  inputData: string = '';
  @Input('label') label: string;
  @Input('type') type: string;
  @Input('errorMsg') errorMsg: string;
  @Input('rows') rows: number;
  @Input('ngModel') ngModel: string;
  @Output() inputChange = new EventEmitter<string>();
  dir: string = 'rtl';
  dirSub: Subscription;


  constructor(
    private langService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
  }

  toggleState() {
    this.state = this.state === 'idial' ? 'focused' : 'idial';
    console.log("input state changed : ", this.state);
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }


}

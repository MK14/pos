import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'CashCard',
  templateUrl: './cash-card.component.html',
  styleUrls: ['../credit-card/credit-card.component.scss', './cash-card.component.scss'],
})
export class CashCardComponent implements OnInit, OnDestroy {

  dir: string = 'rtl';
  dirSub: Subscription;
  @Input('available') available: string;
  constructor(
    private langService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
  }
  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

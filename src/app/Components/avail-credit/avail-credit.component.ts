import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'AvailCredit',
  templateUrl: './avail-credit.component.html',
  styleUrls: ['./avail-credit.component.scss'],
})
export class AvailCreditComponent implements OnInit, OnDestroy {

  @Input('credit') credit;
  dirSub: Subscription;

  dir: string = 'rtl';

  constructor(
    private langService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr'
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

import { Plugins } from '@capacitor/core';

const { Browser } = Plugins;

@Component({
  selector: 'MyIframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.scss'],
})
export class IFrameComponent implements OnInit {

  myUrl: SafeResourceUrl;
  @Input('url') url: string = '';

  constructor(
    private domSanitizer: DomSanitizer
  ) { }

  ngOnInit() {

    // this.myUrl = this.domSanitizer.bypassSecurityTrustUrl(this.url)
    Browser.open({ url: 'http://capacitorjs.com/' });

  }

}

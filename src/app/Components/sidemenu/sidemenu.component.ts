import { Component, OnInit, OnDestroy } from '@angular/core';
import { PageItem } from 'src/app/Models/PageItem.model';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { User } from 'src/app/Models/User.model';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'sideMenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
})
export class SidemenuComponent implements OnInit, OnDestroy {
  appPages: PageItem[] = [
    { title: 'main', url: '/main', icon: 'home' },
    { title: 'wallet', url: '/wallet', icon: 'wallet' },
    { title: 'history', url: '/history', icon: 'reader' },
    { title: 'contactUs', url: '/contactus', icon: 'call' },
  ];

  dir: string = 'rtl';
  dirSub: Subscription;
  user: User = {
    username: '',
    avatarURL: '',
    providerID: '',
    phoneNumber: '',
    _id: '',
    walletID: ''
  };

  constructor(
    private langService: LangService,
    private router: Router
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
    this.user = JSON.parse(localStorage.getItem('POSTrader'));
  }

  changeLang() {
    let nextLang = this.langService.getLang() === 'ar' ? 'en' : 'ar';
    this.langService.changeLanguage(nextLang);
  }

  logOut() {
    localStorage.removeItem("POSTrader");
    this.router.navigate(['/login'], {
      replaceUrl: true
    })
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

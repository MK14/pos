import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'MainHeader',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss'],
})
export class MainHeaderComponent implements OnInit, OnDestroy {

  @Input('title') title;
  dir: string = 'rtl';
  dirSub: Subscription;

  constructor(
    private langService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'en' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir === 'rtl' ? 'ltr' : 'rtl'
    })
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }
}

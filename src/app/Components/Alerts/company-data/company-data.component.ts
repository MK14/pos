import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { Animations } from 'src/app/Shared/animations';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'CompanyDataAlert',
  templateUrl: './company-data.component.html',
  styleUrls: ['./company-data.component.scss'],
  animations: [Animations.bounceUp]

})
export class CompanyDataComponent implements OnInit, OnDestroy {

  @Output('hide') hide = new EventEmitter();
  @Output('action') action = new EventEmitter();


  state: string = 'idial';
  dir: string = 'rtl';
  dirSub: Subscription;
  constructor(
    private langService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe((dir) => {
      this.dir = dir;
    })
  }

  hideModal() {
    this.hide.emit();
  }
  handleAction() {
    this.state = 'loading';
    this.action.emit();

  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Animations } from 'src/app/Shared/animations';

@Component({
  selector: 'LoadDataAlert',
  templateUrl: './load-data.component.html',
  styleUrls: ['./load-data.component.scss'],
  animations: [Animations.bounceUp]

})
export class LoadDataComponent implements OnInit {


  @Output('hide') hide = new EventEmitter();
  @Output('action') action = new EventEmitter();


  constructor() { }

  ngOnInit() { }
  hideModal() {
    this.hide.emit();
  }
  handleAction() {
    this.action.emit();
  }

}

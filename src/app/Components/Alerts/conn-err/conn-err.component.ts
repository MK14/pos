import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Animations } from 'src/app/Shared/animations';
import { Plugins } from '@capacitor/core';

const { App } = Plugins;

@Component({
  selector: 'ConnErrAlert',
  templateUrl: './conn-err.component.html',
  styleUrls: ['./conn-err.component.scss'],
  animations: [Animations.bounceUp]
})
export class ConnErrComponent implements OnInit {

  @Output('hide') hide = new EventEmitter();
  @Output('action') action = new EventEmitter();
  @Output('exitAction') exitAction = new EventEmitter();

  @Input('state') state: string = 'idial'


  constructor() { }

  ngOnInit() { }

  hideModal() {
    this.hide.emit();
  }
  handleAction() {
    this.action.emit();
  }

  handleExit() {
    // this.exitAction.emit();
    App.exitApp();
  }


}

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Animations } from 'src/app/Shared/animations';
import { Plugins } from '@capacitor/core';

const { App } = Plugins;

@Component({
  selector: 'AuthErrAlert',
  templateUrl: './auth-err.component.html',
  styleUrls: ['./auth-err.component.scss'],
  animations: [Animations.bounceUp]
})
export class AuthErrComponent implements OnInit {

  @Output('hide') hide = new EventEmitter();

  @Input('state') state: string = 'idial'


  constructor() { }

  ngOnInit() { }

  hideModal() {
    this.hide.emit();
  }

  handleExit() {
    App.exitApp();
  }


}

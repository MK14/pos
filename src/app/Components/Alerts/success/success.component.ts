import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Animations } from 'src/app/Shared/animations';

@Component({
  selector: 'SuccessAlert',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss'],
  animations: [Animations.bounceUp]
})
export class SuccessComponent implements OnInit {

  @Output('hide') hide = new EventEmitter();
  @Output('action') action = new EventEmitter();
  @Input('msg') msg;

  constructor() { }

  ngOnInit() { }

  hideModal() {
    this.hide.emit();
  }
  handleAction() {
    this.action.emit();
  }

}

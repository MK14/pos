import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';
import { NoteItem } from 'src/app/Models/noteItem.model';

@Component({
  selector: 'NoteItem',
  templateUrl: './note-item.component.html',
  styleUrls: ['./note-item.component.scss'],
})
export class NoteItemComponent implements OnInit, OnDestroy {

  dir: string = 'rtl';
  dirSub: Subscription;
  @Input('noteData')noteData:NoteItem;

  constructor(
    private langeService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langeService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langeService.dir.subscribe(dir => {
      this.dir = dir;
    })
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

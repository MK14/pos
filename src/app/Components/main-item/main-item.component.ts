import { Component, OnInit, Input } from '@angular/core';
import { ScreenItem } from 'src/app/Models/ScreenItem.model';
import { Plugins } from '@capacitor/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform } from '@ionic/angular';

const { Browser } = Plugins;

@Component({
  selector: 'MainItem',
  templateUrl: './main-item.component.html',
  styleUrls: ['./main-item.component.scss'],
})
export class MainItemComponent implements OnInit {

  @Input('screen') screen: ScreenItem;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private plateform: Platform,
  ) { }

  ngOnInit() { }

  handlePress() {
    let { link } = this.screen;
    if (link !== 'others') {
      this.router.navigate([`${link}`], {
        relativeTo: this.route
      })
    } else {
      this.openBrawser()
    }
  }

  openBrawser() {
    Browser.open({ url: this.screen.externalURL })
    Browser.addListener('browserFinished', () => {
      this.backToHome();
    })
  }

  backToHome() {
    this.router.navigate(['/main'], {
      relativeTo: this.route
    })
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'CreditHistory',
  templateUrl: './credit-history.component.html',
  styleUrls: ['./credit-history.component.scss'],
})
export class CreditHistoryComponent implements OnInit, OnDestroy {

  dir: string = 'rtl';
  dirSub: Subscription;

  constructor(
    private langService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
  }
  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}

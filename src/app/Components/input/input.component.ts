import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { LangService } from 'src/app/Services/lang.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'MyInput',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit, OnDestroy {

  state: string = "idial";
  inputData: string = '';
  @Input('label') label: string;
  @Input('type') type: string;
  @Input('errorMsg') errorMsg: string;
  @Input('ngModel') ngModel: string;
  @Output() inputChange = new EventEmitter<string>();
  dir: string = 'rtl';
  dirSub: Subscription

  constructor(
    private langService: LangService
  ) { }

  ngOnInit() {
    this.dir = this.langService.getLang() === 'ar' ? 'rtl' : 'ltr';
    this.dirSub = this.langService.dir.subscribe(dir => {
      this.dir = dir
    })
  }

  clearErr(){
    this.errorMsg = '';
  }

  toggleState() {
    this.state = this.state === 'idial' ? 'focused' : 'idial';
    console.log("input state changed : ", this.state);
  }

  ngOnDestroy() {
    this.dirSub.unsubscribe();
  }

}
